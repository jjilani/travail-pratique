<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$formFields = array(
        'firstName' => array(
            'id' => 'firstName',
            'name' => 'firstName',
            'element' => 'input',
            'type' => 'text',
            'value' => null,
        ),
        'lastName' => array(
            'id' => 'lastName',
            'name' => 'lastName',
            'element' => 'input',
            'type' => 'text',
            'value' => null,
        ),
        'email' => array(
            'id' => 'email',
            'name' => 'email',
            'element' => 'input',
            'type' => 'email',
            'value' => null,
        ),
        'userPassword' => array(
            'id' => 'userPassword',
            'name' => 'userPassword',
            'element' => 'input',
            'type' => 'password',
            'value' => null,
        ),
        'userName' => array(
            'id' => 'userName',
            'name' => 'userName',
            'element' => 'input',
            'type' => 'text',
            'value' => null,
        ),
    );
    function get_DBconnection()
    {
        $mysqli = mysqli_connect('127.0.0.1', 'root', 'abc123...', 'programmationweb3');
        mysqli_set_charset($mysqli, 'utf8');

        return $mysqli;
    }
    function get_query_result($query)
    {
        $mysqli = get_DBconnection();
        $result = mysqli_query($mysqli, $query);
        $collection = array();
        while ($associativeArray = mysqli_fetch_assoc($result)) {
            array_push($collection, $associativeArray);
        }
        mysqli_free_result($result);

        mysqli_close($mysqli);

        return $collection;
    }
    function check_if_username_exists($username, $id = 0)
    {
        $query = 'SELECT * FROM tp_user';
        $list = get_query_result($query);
        $exists = false;
        foreach ($list as $user => $attributes) {
            if ($id !== 0) {
                if ($username === $attributes['userName'] and $id !== $attributes['id']) {
                    $exists = true;
                }
            } else {
                if ($username === $attributes['userName']) {
                    $exists = true;
                }
            }
        }

        return $exists;
    }
    function check_user($userLogin)
    {
        $userID = 0;
        $query = 'SELECT id, userName, userPassword FROM tp_user;';
        $usersList = get_query_result($query);
        foreach ($usersList as $user => $userInfo) {
            if ($userInfo['userName'] === $userLogin['userName'] && password_verify($userLogin['userPassword'], $userInfo['userPassword'])) {
                $userID = $userInfo['id'];
            }
        }

        return $userID;
    }
    function deleteUser($query)
    {
        $mysqli = get_DBconnection();
        $result = mysqli_query($mysqli, $query);
        mysqli_close($mysqli);
    }
    function editUser($idUser)
    {
        $query = "SELECT * FROM tp_user WHERE id LIKE $idUser";
        $result = get_query_result($query);
        $user = $result[0];

        return $user;
    }
    function showAllUsers($users, $connectedUser)
    {
        $classConnectedUser;
        foreach ($users as $user => $userInfo) {
            $classConnectedUser = null;
            if ($connectedUser === $userInfo['userName']) {
                $classConnectedUser = 'bg-light text-dark';
            }
            echo "<tr class='$classConnectedUser'><td>$userInfo[firstName]</td><td>$userInfo[lastName]</td>";
            echo "<td>$userInfo[email]</td><td>$userInfo[creationDate]</td><td>$userInfo[modificationDate]</td>";
            echo "<td><a href='?click=delete&id=$userInfo[id]'><i class='fas fa-trash-alt'> delete</i></a></td>";
            echo "<td><a href='?click=edit&id=$userInfo[id]'><i class='fas fa-edit'></i> edit</a></td></tr>";
        }
    }
    function create_register_form($formFields)
    {
        foreach ($formFields as $field => $attributes) {
            echo create_element_input($attributes);
        }
        echo create_submit_button();
    }
    function create_edit_form($formFields, $values)
    {
        foreach ($formFields as $field => $attributes) {
            if (!($field === 'userPassword')) {
                $attributes['value'] = $values[$field];
            }

            echo create_element_input($attributes);
        }
        echo create_submit_button();
    }
    function create_submit_button()
    {
        $submit = "<div class='form-group row'><div class='col-sm-10 d-flex flex-row'>";
        $submit = $submit."<button type='submit' name='btn-signup' class='btn btn-secondary  p-2'>Sauvgarder</button>";
        $submit = $submit.'</div></div>';

        return $submit;
    }

    function create_element_input($inputElement)
    {
        $input = "<div class='col-sm-10'><input value='$inputElement[value]' class='form-control' placeholder='$inputElement[name]' required type='$inputElement[type]' ";
        $input = $input."name='$inputElement[name]' id='$inputElement[id]'></div>";

        return create_Element($inputElement, $input);
    }
    function create_Element($element, $elementType)
    {
        $divElement = "<div class='form-group row'><label for='$element[id]' class='col-sm-2 col-form-label'>$element[name] :</label>$elementType</div>";

        return $divElement;
    }
    function send_mail_toUser($username)
    {
        // update database with new password
        $querryAll = 'SELECT * FROM tp_user';
        $list = get_query_result($querryAll);
        $email;
        $id;
        $update;
        $RandomPassword = generateRandomString();
        $newPassword = password_hash($RandomPassword, PASSWORD_DEFAULT);
        foreach ($list as $user => $attributes) {
            if ($attributes['userName'] === $username) {
                $email = $attributes['email'];
                $id = $attributes['id'];
                $update = $query = "UPDATE tp_user SET userName = '$attributes[userName]', email='$email', userPassword= '$newPassword',";
                $update = $update." lastName='$attributes[lastName]', firstName='$attributes[firstName]' WHERE id LIKE $id;";
            }
        }
        $msg = '';
        $mysqli = get_DBconnection();
        if (mysqli_query($mysqli, $update)) {
            $msg = "<div class='form-group'>";
            $msg = $msg."<h4 style='color:green'>Congratulation you have successfully Changed your Password.</h4>";
            $msg = $msg."<h4 >nouveau Password envoyer a votre adresse email : <b style='color:red'> $email </b>.</h4>";
            $msg = $msg.'</div>';
        } else {
            $msg = '<h4>Error while registering you...</h4>'.mysqli_error($mysqli);
        }

        mysqli_close($mysqli);
        // prepartion mail and mail server
        require 'src/Exception.php';
        require 'src/PHPMailer.php';
        require 'src/SMTP.php';

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = 'UTF-8';

        $mail->Host = 'ssl://smtp.gmail.com'; // SMTP server example
        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->Port = 465;                    // set the SMTP port for the GMAIL server
        $mail->Username = 'etudiant.isi.java2@gmail.com'; // SMTP account username example
        $mail->Password = 'abc123...';
        $mail->setFrom('TP_PHP-mostapha-jihene@ISI-MTL.COM', 'Mot de passe Oublier ');

        $mail->addAddress("$email", 'Test 1');

        $mail->Subject = 'mot de passe oublier (TP-PHP)';

        $mail->Body = "Ceci est Votre nouveau mot de passe pour vous Loger : $RandomPassword";
        // send mail
        try {
            $mail->send();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $msg;
    }
    function generateRandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
