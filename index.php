<?php

    session_start();
    $usersList;
    if (!isset($_SESSION['userName'])) {
        header('Location: login.php');
    } else {
        include_once 'MyAppFunctions.php';
        $query = 'SELECT * FROM tp_user;';
        $usersList = get_query_result($query);
        if (isset($_GET['click'])) {
            echo $_GET['click'];
            if ($_GET['click'] === 'delete') {
                $query = "DELETE FROM tp_user WHERE id LIKE $_GET[id];";
                deleteUser($query);
                header('Location: index.php');
            } elseif ($_GET['click'] === 'edit') {
                $_SESSION['edit'] = $_GET['id'];
                header('Location: form.php');
            } else {
                $_SESSION['edit'] = null;
                header('Location: form.php?edit=5');
            }
        } else {
            $_SESSION['edit'] = null;
        }
    }

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/8b551ce0fa.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Document</title>
</head>

<body>
    <div class="container" style="padding:50px">
        <nav class="navbar navbar-toggleable-sm navbar-inverse bg-inverse bg-dark">
            <div class="d-flex ">
                <a class="navbar-brand  order-1 text-white" href="#">MySQL User Form</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div id="collapsibleNavbar" class=" order-2">
                    <ul class="list-unstyled  ">
                        <li class="  ">
                            <a class="nav-link text-muted" href="form.php">Ajouter</a>
                        </li>
                    </ul>
                </div>
            </div>
            <ul>
                <li class="nav-item ml-auto">
                    <a class="nav-link text-muted" href="login.php">Logout</a>
                </li>
            </ul>
        </nav>

        <table border="2" class="table table-dark">
            <tr>
                <th> First Name </th>
                <th> Last Name </th>
                <th> E-Mail </th>
                <th> Creation Date </th>
                <th>Modification Date</th>
                <th> DELETE </th>
                <th> EDIT</th>
            </tr>
            <?php
            showAllUsers($usersList, $_SESSION['userName']);
        ?>
        </table>
    </div>
</body>

</html>