<?php
    session_start();
    $mode;
    $user;
    $msgExists;
    require_once 'MyAppFunctions.php';

    require_once 'MyAppFunctions.php';
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_SESSION['edit'])) {
            $mode = 'EDIT_USER';
        } else {
            $mode = 'NEW_USER';
        }
        $uname = $_POST['userName'];
        $email = $_POST['email'];
        $upass = password_hash($_POST['userPassword'], PASSWORD_DEFAULT);
        $lname = $_POST['lastName'];
        $fname = $_POST['firstName'];
        $query;
        $exists;
        if ($mode === 'EDIT_USER') {
            $exists = check_if_username_exists($uname, $_SESSION['edit']);
            $user = editUser($_SESSION['edit']);
        } elseif ($mode === 'NEW_USER') {
            $exists = check_if_username_exists($uname);
        }
        if ($exists) {
            $msgExists = "<b style='color:red'>UserName DEJA UTILISER</b>";
            $_SESSION['msgExists'] = $msgExists;
        } else {
            $_SESSION['msgExists'] = null;
            if ($mode === 'EDIT_USER') {
                $query = "UPDATE tp_user SET userName = '$uname', email='$email', userPassword= '$upass', lastName='$lname', firstName='$fname' WHERE id LIKE '$_SESSION[edit]';";
            } elseif ($mode === 'NEW_USER') {
                $query = "INSERT INTO tp_user(username,email,userPassword,lastName,firstName) VALUES('$uname','$email','$upass','$lname','$fname')";
            }
            define('HOME_URL', MYSQLI_STORE_RESULT);
            $mysqli = get_DBconnection();
            if (mysqli_query($mysqli, $query)) {
                $msg = "<h4 style='color:green'>Congratulation you have successfully registered.</h4>";
            } else {
                $msg = "<h4 style='color:red'>Error while registering you...</h4>".mysqli_error($mysqli);
            }
            mysqli_close($mysqli);
            header('refresh:3;url=index.php');
        }
    } else {
        if (isset($_SESSION['edit'])) {
            $user = editUser($_SESSION['edit']);
            $mode = 'EDIT_USER';
        } else {
            $mode = 'NEW_USER';
        }
    }

?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Registration </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

</head>

<body>
    <div class="container" style="padding:100px">
        <center>
            <nav class="navbar navbar-toggleable-sm navbar-inverse bg-inverse bg-dark">
                <div class="d-flex ">
                    <a class="navbar-brand  order-1 text-white" href="#">MySQL User Form</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#collapsibleNavbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div id="collapsibleNavbar" class=" order-2">
                        <ul class="list-unstyled  ">
                            <li class="  ">
                                <a class="nav-link text-muted" href="#">Ajouter</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <ul>
                    <li class="nav-item ml-auto">
                        <a class="nav-link text-muted" href=" login.php">Logout</a>
                    </li>
                </ul>
            </nav>
            <div id="login-form">
                <form action="form.php" method="post">
                    <?php
                    if (isset($_SESSION['msgExists'])) {
                        echo $_SESSION['msgExists'].'</br>';
                    }
                    echo @$msg.'</br>';
                if ($mode === 'EDIT_USER') {
                    create_edit_form($formFields, $user);
                } elseif ($mode === 'NEW_USER') {
                    create_register_form($formFields);
                }
                ?>
                </form>
            </div>
        </center>
    </div>
</body>

</html>