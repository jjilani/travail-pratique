<?php
include_once 'MyAppFunctions.php';
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        require_once 'MyAppFunctions.php';
        $userName = $_POST['usernamePass'];
        if (check_if_username_exists($userName)) {
            $query = "SELECT id, email FROM tp_user WHERE userName LIKE $userName";

            $msg = send_mail_toUser($userName);
        } else {
            $msg = ' USER N ESXISTE PAS';
            header('refresh:3');
        }

        header('refresh:5;url=login.php');
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script src="https://kit.fontawesome.com/8b551ce0fa.js"></script>
    <title>Document</title>
</head>

<body>
    <div class="container" style="padding:150px">
        <h4> Enter your userName : vous recevez un mot de passe temporaire </h4>
        <br>
        <br>
        <form action="forgotPassword.php" method="post">
            <?php
            if (isset($msg)) {
                echo $msg;
            }  ?>
            <div class="form-group">
                <i class="fas fa-user"></i>
                <label for="username"> Enter your Username :</label>
                <input type="text" class="form-control" id="username" placeholder="Enter username" name="usernamePass"
                    required>
            </div>

            <button type="submit" class="btn btn-primary"> Send new password </button>
        </form>
    </div>
</body>

</html>