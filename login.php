<?php
    include_once 'MyAppFunctions.php';
    $isValid = true;
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $username = $_POST['username'];
        $userpassword = $_POST['password'];
        $userLogin = ['userName' => $username, 'userPassword' => $userpassword];
        $userID = check_user($userLogin);

        if ($userID !== 0) {
            session_start();
            $_SESSION['userName'] = $username;
            $_SESSION['userID'] = $userID;
            $isValid = true;
            header('Location: index.php');
        } else {
            $isValid = false;
        }
    }

?>
<!DOCTYPE html>
<html>

<head>
    <title>Login Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script src="https://kit.fontawesome.com/8b551ce0fa.js"></script>
</head>

<body>
    <div class="container" style="padding:150px">
        <h3>L'Usager par default est <b>Username: admin </b> et <b> Password: admin</b></h3>
        <h5>
            <?php
        if (!$isValid) {
            echo "<span style='color:red'>Veuillez Entrer des informations valides SVP !!!</span>";
        }
        ?>
        </h5>
        <form action="login.php" method="POST">
            <div class="form-group">
                <i class="fas fa-user"></i>
                <label for="uname"> Username:</label>
                <input type="text" class="form-control" id="username" placeholder="Enter username" name="username"
                    required>
            </div>
            <div class="form-group">
                <i class="fas fa-key"></i>
                <label for="pwd"> Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Enter password" name="password"
                    required>
            </div>
            <div class="form-group">
                <a href="forgotPassword.php"> mot de passe oublier ?</a>
            </div>
            <button type="submit" class="btn btn-primary"> Login </button>
        </form>
    </div>

</body>

</html>